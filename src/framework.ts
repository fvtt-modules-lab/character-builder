export interface StepStatus {}

/**
 * Extended result value from checking step completion
 */
export interface CheckResult {
  completed: boolean;
  /**
   * A short (max 5 words) description of what the (un)successful value is or means.
   */
  details?: string;
}

export interface BuilderStep {
  /**
   * Name for this step, required to be unique within one step sequence
   * String needs to be a valid css class name
   * localised as step title (CHARACTER_BUILDER.<stepName>)
   */
  stepName: string;
  /**
   * Path to a template, needs to be preloaded (fix this on load probs)
   */
  template?: string;
  /**
   * Straight text description - insted of, or in addition to, the template.
   * Can be a localisable string. May contain HTML.
   */
  description?: string;
  /**
   * A condition function for checking whether the step is completed
   */
  check: (character: Actor) => CheckResult | boolean;

  /**
   * Dynamically used if this step supplies a template
   */
  getData?: (character: Actor) => any;
  /**
   * Automatically used if the template contains a form
   */
  onFormChange?: (form: JQuery<HTMLFormElement>, character: Actor) => void;
  /**
   * Automatically used if the template contains a form
   */
  onFormSubmit?: (form: JQuery<HTMLFormElement>, character: Actor) => void;
}
