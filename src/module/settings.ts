export const MODULE_NAME = "character-builder"; // TODO: Better handling

export enum settings {
  OPEN_BUTTON_IN_DIRECTORY = "openButtonInSidebarDirectory",
}

interface ICallbacks {
  [setting: string]: (value: any) => void;
}

const noop = () => {};

interface ModuleSetting {
  setting: settings;
  /** Name of the setting that shows up in the list */
  name: string;
  /** Longer description of the setting that shows up in the list */
  hint: string;
  type: any;
  /** Default value */
  default: any;
  /** Creates a multiple choice dropdown with key: value */
  choices?: Object;
  /** Setting should show up in the settings window */
  config?: boolean;
  /** What scope the setting is saved in */
  scope?: "client" | "world";
}

const moduleSettings: ModuleSetting[] = [];

function registerSetting(callbacks: ICallbacks, { setting, ...options }) {
  game.settings.register(MODULE_NAME, setting, {
    config: true,
    scope: "client",
    ...options,
    onChange: callbacks[setting] || noop,
  });
}

export function registerSettings(callbacks: ICallbacks = {}) {
  moduleSettings.forEach(item => {
    registerSetting(callbacks, item);
  });
}

export function getSetting(setting: settings) {
  return game.settings.get(MODULE_NAME, setting as string);
}

export function setSetting(setting: settings, value: any) {
  return game.settings.set(MODULE_NAME, setting as string, value);
}
