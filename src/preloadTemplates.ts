export const preloadTemplates = async function () {
  const templatePaths = [
    "modules/character-builder/templates/builder-window.hbs",
    "modules/character-builder/templates/dnd5e/ability-scores.hbs",
  ];
  return loadTemplates(templatePaths);
};
