import { getSetting, settings, registerSettings } from "./module/settings.js";
import { BuilderStep, CheckResult } from "./framework.js";
import { preloadTemplates } from "./preloadTemplates.js";

import { dnd5eSteps } from "./dnd5eBuilder.js";

interface RenderBuilderStep extends BuilderStep {
  result: CheckResult;
  locName: string;
  templateData?: any;
}

class BuilderWindow extends Application {
  steps: BuilderStep[];
  results: { [name: string]: CheckResult } = {};
  character: Actor = null;

  updateHooks = false;

  constructor(steps: BuilderStep[], char: Actor) {
    super({
      title: "Character Builder",
      id: "character-builder",
      template: "modules/character-builder/templates/builder-window.hbs",
      resizable: true,
      width: 400,
    });
    this.steps = steps;
    this.character = char;
  }

  setHooks() {
    if (!this.updateHooks) {
      Hooks.on("createOwnedItem", this.onUpdated);
      Hooks.on("deleteOwnedItem", this.onUpdated);
      Hooks.on("updateOwnedItem", this.onUpdated);
      Hooks.on("updateActor", this.onUpdated);
      this.updateHooks = true;
    }
  }

  clearHooks() {
    if (this.updateHooks) {
      Hooks.off("createOwnedItem", this.onUpdated);
      Hooks.off("deleteOwnedItem", this.onUpdated);
      Hooks.off("updateOwnedItem", this.onUpdated);
      Hooks.off("updateActor", this.onUpdated);
      this.updateHooks = false;
    }
  }

  onUpdated = (entity: Entity) => {
    if (entity.id === this.character.id) {
      console.log("oof");
      this.render(true);
    }
  };

  render(force) {
    this.setHooks();
    this.validate();
    console.log(this.results);
    return super.render(force);
  }

  close() {
    this.clearHooks();
    return super.close();
  }

  getData() {
    return {
      characterName: this.character.name,
      steps: this.steps.map(step => {
        let stepData: RenderBuilderStep = {
          ...step,
          result: this.results[step.stepName],
          locName: "CHARACTER_BUILDER." + step.stepName,
        };
        if (step.template && step.getData) {
          stepData.templateData = step.getData(this.character);
        }
        return stepData;
      }),
      characterUuid: this.character.uuid,
    };
  }

  protected activateListeners(html: JQuery<HTMLElement>): void {
    for (const elem of html.find(".steps").find("form")) {
      const form = $(elem) as JQuery<HTMLFormElement>;
      const step = form.closest("section[data-step]").data("step");
      const stepStruct = this.steps.find(
        stepStruct => stepStruct.stepName === step
      );

      if (stepStruct.onFormSubmit) {
        form.on("submit", evt => {
          evt.preventDefault();
          stepStruct.onFormSubmit(form, this.character);
        });
      } else {
        form.on("submit", evt => evt.preventDefault());
      }

      if (stepStruct.onFormChange) {
        form.change(evt => {
          stepStruct.onFormChange(form, this.character);
        });
      }
    }
  }

  validate() {
    this.results = {};
    this.steps.forEach(step => {
      const res = step.check(this.character);
      if (typeof res === "boolean") {
        this.results[step.stepName] = { completed: res };
      } else {
        this.results[step.stepName] = res;
      }
    });
  }
}

Hooks.once("init", async function () {
  registerSettings();
});

Hooks.once("ready", async function () {
  console.log("Character Builder | Initializing");
  const buidler = new BuilderWindow(
    dnd5eSteps,
    game.actors.get("5uANJQLOHtLxzr4P")
  );
  await preloadTemplates();
  buidler.render(true);
});
