import { BuilderStep } from "./framework.js";

interface AttrVal {
  value: number;
  racial: number;
}

function structureAttributes(formValues: JQuery.NameValuePair[]) {
  let attributes: Map<string, AttrVal> = new Map();
  Object.keys(game.dnd5e.config.abilities).forEach(k =>
    attributes.set(k, { value: 0, racial: 0 })
  );
  for (const value of formValues) {
    const [t, attr] = value.name.split("_");
    if (t === "attr") {
      attributes.get(attr).value = parseInt(value.value);
    } else if (t === "racial") {
      attributes.get(attr).racial = parseInt(value.value);
    }
  }
  return attributes;
}

function valTotal(val: AttrVal): number {
  return Math.max(1, val.value + val.racial);
}
function valBonus(val: AttrVal) {
  return Math.floor((valTotal(val) - 10) / 2);
}

export const dnd5eSteps: BuilderStep[] = [
  {
    stepName: "dnd5e_chooseRace",
    description: "CHARACTER_BUILDER.dnd5e_chooseRace_description",
    check: (char: Actor) => {
      return {
        completed: char.data.data.details.race !== "",
        details: char.data.data.details.race,
      };
    },
  },
  {
    stepName: "dnd5e_chooseClass",
    description: `
<button onClick="game.packs.get('dnd5e.classes').render(true)">Browse classes</button>
<p>Add class features</p>
<button onClick="game.packs.get('dnd5e.classfeatures').render(true)">Browse class features</button>`,
    check: (char: Actor) => {
      const classes = char.items
        .filter(item => {
          return item.data.type == "class";
        })
        .map(item => {
          return item.data.subclass
            ? `${item.data.name} (${item.data.subclass})`
            : item.data.name;
        });
      console.log(classes);
      return { completed: classes.length != 0, details: classes.join(", ") };
    },
  },
  {
    stepName: "dnd5e_abilityScores",
    template: "modules/character-builder/templates/dnd5e/ability-scores.hbs",
    check: (char: Actor) => {
      return { completed: char.data.data.details.background !== "" };
    },
    getData: (char: Actor) => {
      return {
        abilities: game.dnd5e.config.abilities,
      };
    },
    onFormSubmit(form, character) {
      const attributes = structureAttributes(form.serializeArray());

      let updates = {};
      for (const [attr, values] of attributes) {
        updates[`data.abilities.${attr}.value`] = valTotal(values);
      }
      character.update(updates);
    },
    onFormChange(form, character) {
      const attributes = structureAttributes(form.serializeArray());

      for (const [attr, values] of attributes) {
        const row = form.find(`tr[data-attribute="${attr}"]`);

        row.find(".total").html(`${valTotal(values)}`);
        const bonus = valBonus(values);
        if (bonus >= 0) {
          row.find(".modifier").html(`+${bonus}`);
        } else {
          row.find(".modifier").html(`${bonus}`);
        }
      }
    },
  },
  {
    stepName: "dnd5e_chooseBackground",
    check: (char: Actor) => {
      return {
        completed: char.data.data.details.background !== "",
        details: char.data.data.details.background,
      };
    },
  },
];
